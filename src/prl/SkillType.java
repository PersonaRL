public enum SkillType
{
    PHYSICAL,
    FIRE,
    ICE,
    ELECTRICITY,
    WIND,
    LIGHT,
    DARKNESS,
    ALMIGHTY,

    AILMENT,
    RESTORATION,
    SUPPORT,
    PASSIVE;
}