public abstract class Item
{
    abstract String name;
    abstract String description;

    public Item( String name, String description )
    {
        this.name              = name;
        this.description       = description;
    }

    public abstract int calculateValue();

    public String generateEffectDescription( Effect effect, int effectMagnitude )
    {
        String effectDescription;
        //Large switch case goes here.
    }
}