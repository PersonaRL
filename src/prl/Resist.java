package prl;

public class Resist
{
    int physical;
    int fire;
    int ice;
    int electricity;
    int wind;
    int light;
    int darkness;

    int poison;
    int rage;
    int dizzy;
    int confusion;
    int enervation;
    int exhaustion;
    int silence;
    int fear;
    int unconscious;

    int counter;

    public Resist()
    {
        this.physical    = 0;
        this.fire        = 0;
        this.ice         = 0;
        this.electricity = 0;
        this.wind        = 0;
        this.light       = 0;
        this.darkness    = 0;

        //The value of 70 is a placeholder value.

        this.poison      = 70;
        this.rage        = 70;
        this.dizzy       = 70;
        this.confusion   = 70;
        this.enervation  = 70;
        this.exhaustion  = 70;
        this.silence     = 70;
        this.fear        = 70;
        this.unconscious = 70;

        this.counter     = 0;
    }
}