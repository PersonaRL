package prl.ui;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.GraphicsConfiguration;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Hashtable;
import javax.imageio.ImageIO;

class Tile
{
    private BufferedImage tileImage;
    private Character     character;
    private Color         foregroundColor;

    private static BufferedImage tileset;
    private static Color         colorKey;
    private static int           tileWidth;
    private static int           tileHeight;

    private static Hashtable<Color,Hashtable<Character,Tile>> tiles;
    private static HashSet<Color> possibleColors;

    static
    {
        staticInit();
    }

    private Tile(BufferedImage bi)
    {
        this.tileImage = bi;
    }

    private void setTileImage(BufferedImage bi)
    {
        this.tileImage = bi;
    }

    private void setCharacter(Character ch)
    {
        this.character = ch;
    }

    private void setForegroundColor(Color c)
    {
        this.foregroundColor = c;
        
        for (int w = 0; w < tileWidth; w++) {
            for (int h = 0; h < tileHeight; h++) {
                if ( (this.tileImage.getRGB(w,h) & 0x00FFFFFF ) != 0x00000000) {
                    this.tileImage.setRGB(w, h, this.foregroundColor.getRGB());
                }
            }
        }
    }

    public BufferedImage getTileImage()
    {
        return this.tileImage;
    }

    public Character getCharacter()
    {
        return this.character;
    }

    public Color getForegroundColor()
    {
        return this.foregroundColor;
    }

    public static int getTileWidth()
    {
        return tileWidth;
    }

    public static int getTileHeight()
    {
        return tileHeight;
    }

    private void applyColorKey()
    {
        for (int w = 0; w < tileWidth; w++) {
            for (int h = 0; h < tileHeight; h++) {
                if ( (this.tileImage.getRGB(w,h) & 0x00FFFFFF) == (colorKey.getRGB() & 0x00FFFFFF ) ) {
                    this.tileImage.setRGB(w,h,0x00000000);
                }
            }
        }
    }

    private static void staticInit()
    {
        try
        {
            tileset = ImageIO.read(new File("tileset.bmp"));

            // ImageIO.read() may return null in some cases.
            if (tileset == null) {
                throw new NullPointerException("No registed ImageReader is able to read tileset.bmp. Aborting.");
            }

        } catch (IOException e)
        {
            throw new RuntimeException("An error occured while reading tileset.bmp. Aborting.");
        }

        tileWidth  = tileset.getWidth()/16;
        tileHeight = tileset.getHeight()/16;

        possibleColors = new HashSet<Color>();
        possibleColors.add(Color.LIGHT_GRAY);
        possibleColors.add(Color.YELLOW);
        possibleColors.add(Color.MAGENTA);
        possibleColors.add(Color.GREEN);
        possibleColors.add(Color.RED);

        tiles = new Hashtable<Color,Hashtable<Character,Tile>>(possibleColors.size());

        // Color key is pure pink.
        colorKey = new Color(0xFFFF00FF);

        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsConfiguration gc = ge.getDefaultScreenDevice().getDefaultConfiguration();
        int transparency = tileset.getColorModel().getTransparency();

        for (Color c : possibleColors)
        {
            tiles.put(c, new Hashtable<Character,Tile>(256));

            for (int i = 0; i < 16; i++) {
                for (int j = 0; j < 16; j++) {
                    BufferedImage tileImg = gc.createCompatibleImage(tileWidth, tileHeight, transparency);
                    Graphics2D tileGC = tileImg.createGraphics();
    
                    tileGC.drawImage(tileset, 0, 0, tileWidth, tileHeight, 8*j, 12*i, 8*j+tileWidth, 12*i+tileHeight, null);
                    tileGC.dispose();
                  
                    Tile tile = new Tile(tileImg);
                    tile.applyColorKey();
                    tile.setForegroundColor(c);

                    byte[] b = new byte[1];
                    b[0] = (byte) (16*i+j);
                    ByteBuffer buf = ByteBuffer.wrap(b);
                    char ch = Charset.forName("cp437").decode(buf).charAt(0);
                    tile.setCharacter(ch);
                    tiles.get(c).put(ch, tile);
                }
            }
        }
    }

    public static Tile getTile(char ch)
    {
        return getTile(ch, Color.LIGHT_GRAY);
    }

    public static Tile getTile(char ch, Color c)
    {
        // if c not in possibleColors...
        
        return tiles.get(c).get(ch);
    }
}
