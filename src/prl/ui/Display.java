package prl.ui;

import java.awt.Component;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Hashtable;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

import prl.ui.Tile;

public class Display extends Component
{
    private int width;
    private int height;
    private int pixelWidth;
    private int pixelHeight;


    private Tile[][]  displayTiles;
    private char[][]  displayChars;
    private Color[][] displayColors;

    public Display(int w, int h)
    {
        this.width = w;
        this.height = h;

        this.pixelWidth = this.width * Tile.getTileWidth();
        this.pixelHeight = this.height * Tile.getTileHeight();

        displayTiles  = new Tile[this.width][this.height];
        displayChars  = new char[this.width][this.height];
        displayColors = new Color[this.width][this.height];

        for (int i = 0; i < this.width; i++) {
            for (int j = 0; j < this.height; j++) {
                displayTiles[i][j]  = Tile.getTile(' ');
                displayChars[i][j]  = ' ';
                displayColors[i][j] = Color.LIGHT_GRAY;
            }
        }
    }

    public void setTile(int x, int y, char ch)
    {
        setTile(x, y, ch, Color.LIGHT_GRAY);
    }

    public void setTile(int x, int y, char ch, Color c)
    {
        displayChars[x][y] = ch;
        displayColors[x][y] = c;
    }

    public void setTileColor(int x, int y, Color c)
    {
        displayColors[x][y] = c;
    }

    public int getWidth()
    {
        return this.width;
    }

    public int getHeight()
    {
        return this.height;
    }

    public int getPixelWidth()
    {
        return this.pixelWidth;
    }

    public int getPixelHeight()
    {
        return this.pixelHeight;
    }

    public void drawString(int x, int y, String s)
    {
        int toPrint = s.length();

        if (x < 0 || y > this.height || y < 0) {
            // No part of the string may be printed on the screen.
            throw new IndexOutOfBoundsException("String to be printed is entirely out of bounds.");
        }

        if ( (x + s.length()) > this.width ) {
            // String may be partially printed.
            Logger.getLogger(Logger.GLOBAL_LOGGER_NAME).warning("String \"" + s + "\" will be partially printed at x=" + x + ", y=" + y + ".\n");
            toPrint = this.width - x;
        }

        for (int i = 0; i < toPrint; i++) {
            displayChars[x++][y] = s.charAt(i);
        }
    }

    public void paint(Graphics g)
    {
        Graphics2D g2d = (Graphics2D) g;

        g2d.setClip(0, 0, this.pixelWidth, this.pixelHeight);
        //g2d.setBackground(Color.BLACK);

        for (int w = 0; w < this.width; w++) {
            for (int h = 0; h < this.height; h++) {
                if (displayChars[w][h] == ' ') {
                    continue;
                }
                displayTiles[w][h] = Tile.getTile(displayChars[w][h], displayColors[w][h]);
                g2d.drawImage(displayTiles[w][h].getTileImage(), 8*w, 12*h, null);
                displayChars[w][h] = ' ';
                displayColors[w][h] = Color.LIGHT_GRAY;
            }
        }
    }
}
