package prl;

enum ResistAffinity
{
    WEAK,
    STRONG,
    NULLIFY,
    DRAIN,
    REPEL,
    NONE;
}

public class SpecialResist
{

    ResistAffinity physical;
    ResistAffinity fire;
    ResistAffinity ice;
    ResistAffinity electricity;
    ResistAffinity wind;
    ResistAffinity light;
    ResistAffinity darkness;

    public SpecialResist()
    {
        this.physical    = ResistAffinity.NONE;
        this.fire        = ResistAffinity.NONE;
        this.ice         = ResistAffinity.NONE;
        this.electricity = ResistAffinity.NONE;
        this.wind        = ResistAffinity.NONE;
        this.light       = ResistAffinity.NONE;
        this.darkness    = ResistAffinity.NONE;
    }

    public SpecialResist(ResistAffinity val)
    {
        this.physical    = val;
        this.fire        = val;
        this.ice         = val;
        this.electricity = val;
        this.wind        = val;
        this.light       = val;
        this.darkness    = val;
    }

    public SpecialResist( ResistAffinity physical,  ResistAffinity fire,  ResistAffinity ice,     ResistAffinity electricity,
                          ResistAffinity wind,      ResistAffinity light, ResistAffinity darkness )
    {
        this.physical    = physical;
        this.fire        = fire;
        this.ice         = ice;
        this.electricity = electricity;
        this.wind        = wind;
        this.light       = light;
        this.darkness    = darkness;
    }
}