public class Weapon extends Item
{
    int attack;
    int hitRate;

    Effect effect;
    int    effectMagnitude;
    String effectDescription;

    public Weapon( String name, String description,  int attack, int hitRate, Effect effect, int effectMagnitude, String effectDescription )
    {
        super( name, description );
        this.attack  = attack;
        this.hitRate = hitRate;

        this.effect            = effect;
        this.effectMagnitude   = effectMagnitude;
        this.effectDescription = super.generateEffectDescription( effect, effectMagnitude );
    }

    public int calculateValue()
    {
        //Formula for calculating value...
    }
}