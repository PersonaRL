package prl;

public class Stats
{
    int strength;
    int magic;
    int endurance;
    int agility;
    int luck;

    int strengthPriority;
    int magicPriority;
    int endurancePriority;
    int agilityPriority;
    int luckPriority;

    public static class Builder
    {
        int strength  = 1;
        int magic     = 1;
        int endurance = 1;
        int agility   = 1;
        int luck      = 1;

        int strengthPriority  = 20;
        int magicPriority     = 20;
        int endurancePriority = 20;
        int agilityPriority   = 20;
        int luckPriority      = 20;


        //Required fields for creating a Stats object.
        public Builder( int strength, int magic, int endurance, int agility, int luck )
        {
            this.strength  = strength;
            this.magic     = magic;
            this.endurance = endurance;
            this.agility   = agility;
            this.luck      = luck;
        }

        //Optional fields for creating a Stats onject. If not configured, defualt priority of 20 is given.
        public Builder priority( int strengthPriority, int magicPriority, int endurancePriority, int agilityPriority, int luckPriority )
        {
            this.strengthPriority  = strengthPriority;
            this.magicPriority     = magicPriority;
            this.endurancePriority = endurancePriority;
            this.agilityPriority   = agilityPriority;
            this.luckPriority      = luckPriority;
            return this;
        }

        public Stats build()
        {
            return new Stats( this );
        }
    }

    public Stats( Builder builder )
    {
        this.strength  = builder.strength;
        this.magic     = builder.magic;
        this.endurance = builder.endurance;
        this.agility   = builder.agility;
        this.luck      = builder.luck;

        this.strengthPriority  = builder.strengthPriority;
        this.magicPriority     = builder.magicPriority;
        this.endurancePriority = builder.endurancePriority;
        this.agilityPriority   = builder.agilityPriority;
        this.luckPriority      = builder.luckPriority;
    }
}