public class Accessory extends Item
{
    Effect effect;
    int    effectMagnitude;
    String effectDescription;

    public Accessory( String name, String description, String effectDescription )
    {
        super( name, description );

        this.effect            = effect;
        this.effectMagnitude   = effectMagnitude;
        this.effectDescription = super.generateEffectDescription( effect, effectMagnitude );
    }

    public int calculateValue()
    {
        //Formula for calculating value...
    }
}