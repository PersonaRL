public enum EquipEffect
{
    NONE,

    //In percentage.
    HP_BONUS,
    HP_BONUS_PERCENT,
    SP_BONUS,
    SP_BONUS_PERCENT,
    HP_REGEN,
    SP_REGEN,

    //In absolute values.
    STRENGTH_BONUS,
    MAGIC_BONUS,
    ENDURANCE_BONUS,
    AGILITY_BONUS,
    LUCK_BONUS,
    STATS_BONUS,        //Grants a bonus to all stats.

    //In percentage.
    PHYSICAL_RESIST,
    FIRE_RESIST,
    ICE_RESIST,
    ELECTRICITY_RESIST,
    WIND_RESIST,
    LIGHT_RESIST,
    DARKNESS_RESIST,
    ELEMENTAL_RESIST,   //Grants a resistance bonus against all elements.

    //In percentage.
    POISON_RESIST,
    RAGE_RESIST,
    DIZZY_RESIST,
    CONFUSION_RESIST,
    ENERVATION_RESIST,
    EXHAUSTION_RESIST,
    SILENCE_RESIST,
    FEAR_RESIST,
    UNCONSCIOUS_RESIST,
    AILMENT_RESIST,     //Grants a resistance bonus against all ailments.

    COUNTER;
}


