public class Armor extends Item
{
    int defense;
    int evadeRate;

    Effect effect;
    int    effectMagnitude;
    String effectDescription;

    public Armor( String name, String description, String effectDescription, int defense, int evadeRate )
    {
        super( name, description );
        this.defense   = defense;
        this.evadeRate = evadeRate;

        this.effect            = effect;
        this.effectMagnitude   = effectMagnitude;
        this.effectDescription = super.generateEffectDescription( effect, effectMagnitude );
    }

    public int calculateValue()
    {
        //Formula for calculating value...
    }
}