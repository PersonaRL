package prl;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Logger;

import prl.ui.Display;

public class Game implements KeyListener
{
    private volatile static Game uniqueInstance;

    /* tmp */
    Point playerPosition;

    /* UI related */
    private Frame   frame;
    private Display display;

    /* Game related? */
    private boolean running;

    private Game()
    {
        /* Initializing UI */
        frame = new Frame("PersonaRL");
        display = new Display(80, 30);

        frame.setBackground(Color.BLACK);
        frame.add(display);
        frame.addKeyListener(this);
        int w = display.getPixelWidth() + (display.getPixelWidth() - display.getSize().width);    // TODO
        int h = display.getPixelHeight() + (display.getPixelHeight() - display.getSize().height); // TODO
        frame.setSize(640+10-6, 360+30-3);
        frame.setResizable(false);
        frame.setVisible(true);       
        frame.createBufferStrategy(2);

        frame.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        playerPosition = new Point(0, 0);

        running = true;

        Logger.getLogger(Logger.GLOBAL_LOGGER_NAME).info("frame.isDoubleBuffered():" + frame.isDoubleBuffered());
    }

    public static Game getInstance()
    {
        if (uniqueInstance == null) {
            synchronized (Game.class) {
                if (uniqueInstance == null) {
                    uniqueInstance = new Game();
                }
            }
        }

        return uniqueInstance;
    }

    public boolean isRunning()
    {
        return this.running;
    }

    public void keyPressed(KeyEvent e)
    {
        Logger.getLogger(Logger.GLOBAL_LOGGER_NAME).info("Key pressed: " + e.getKeyText(e.getKeyCode()) + '\n');
        switch (e.getKeyCode()) {
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_KP_LEFT:
                playerPosition.x--;
                if (playerPosition.x < 0) {
                    playerPosition.x = 0;
                }
                break;
                
            case KeyEvent.VK_UP:
            case KeyEvent.VK_KP_UP:
                playerPosition.y--;
                if (playerPosition.y < 0) {
                    playerPosition.y = 0;
                }
                break;

            case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_KP_RIGHT:
                playerPosition.x++;
                if (playerPosition.x == display.getWidth()) {
                    playerPosition.x = display.getWidth()-1;
                }
                break;

            case KeyEvent.VK_DOWN:
            case KeyEvent.VK_KP_DOWN:
                playerPosition.y++;
                if (playerPosition.y == display.getHeight()) {
                    playerPosition.y = display.getHeight()-1;
                }
                break;
        }
        e.consume();

        logic();
    }

    public void keyReleased(KeyEvent e)
    {
        Logger.getLogger(Logger.GLOBAL_LOGGER_NAME).info("Key released: " + e.getKeyText(e.getKeyCode()) + '\n');
        e.consume();
    }

    public void keyTyped(KeyEvent e)
    {
        e.consume();
    }

    public void logic()
    {
        Logger.getLogger(Logger.GLOBAL_LOGGER_NAME).info("Player position: (" + playerPosition.x + ',' + playerPosition.y + ")\n");
        display.setTile(playerPosition.x, playerPosition.y, '@', Color.BLUE);
        display.repaint();
    }
}
