package prl;

private enum ConsumableEffect
{
    /* Some P4 items will not be implemented here, such as Chest Keys and Homunculus.
    They can implemented through a method that checks for each item when needed.*/

    RESTORE_HP,
    RESTORE_HP_PERCENT,
    RESTORE_SP,
    RESTORE_SP_PERCENT,

    CURE_POISON,
    CURE_RAGE,
    CURE_CONFUSION,
    CURE_ENERVATION,
    CURE_EXHAUSTION,
    CURE_SILENCE,
    CURE_FEAR,

    ESCAPE_DUNGEON,  //Placeholder for Goho-M.
    TELEPORT,

    REFLECT_PHYSICAL,
    REFLECT_MAGIC,
    INCREASE_ATTACK,
    INCREASE_HIT_EVADE,
    INCREASE_DEFENSE,
    NULLIFY_ENEMY_BONUSES,
    NULLIFY_PLAYER_PENALTIES,




public class Consumable extends Item
{
    private ConsumableEffect consumableEffect;
    private int              consumableMagnitude;

    public Consumable( String name, String description, ConsumableEffect consumableEffect )
    {
        super( name, description );
        this.consumableEffect    = consumableEffect;
        this.consumableMagnitude = 0;
    }

    public Consumable( String name, String description, ConsumableEffect consumableEffect, int consumableMagnitude )
    {
        super( name, description );
        this.consumableEffect    = consumableEffect;
        this.consumableMagnitude = consumableMagnitude;
    }

    public use( String name )
    {
        //Large switch case which applies a consumable's effect.
        //Remove item from inventory.
    }
}