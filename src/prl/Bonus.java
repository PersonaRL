package prl;

public class Bonus
{
    int strength;
    int magic;
    int endurance;
    int agility;
    int luck;

    int physical;
    int fire;
    int ice;
    int electricity;
    int wind;
    int light;
    int darkness;

    int poison;
    int rage;
    int dizzy;
    int confusion;
    int enervation;
    int exhaustion;
    int silence;
    int fear;
    int unconscious;

    int counter;

    public static class Builder
    {
        Bonus bonus;

        public Builder()
        {
            bonus = new Bonus();
        }

        //All bonuses to stats are tended to below.
        public Builder strength( int val )
        {
            this.bonus.strength = val;
            return this;
        }

        public Builder magic( int val )
        {
            this.bonus.magic = val;
            return this;
        }

        public Builder endurance( int val )
        {
            this.bonus.endurance = val;
            return this;
        }

        public Builder agility( int val )
        {
            this.bonus.agility = val;
            return this;
        }

        public Builder luck( int val )
        {
            this.bonus.luck = val;
            return this;
        }

        public Builder stats( int val )
        {
            this.bonus.strength  = val;
            this.bonus.magic     = val;
            this.bonus.endurance = val;
            this.bonus.agility   = val;
            this.bonus.luck      = val;
            return this;
        }

        //All bonuses to elemental resistances are tended to below.
        public Builder physical( int val )
        {
            this.bonus.physical = val;
            return this;
        }

        public Builder fire( int val )
        {
            this.bonus.fire = val;
            return this;
        }

        public Builder ice( int val )
        {
            this.bonus.ice = val;
            return this;
        }

        public Builder electricity( int val )
        {
            this.bonus.electricity = val;
            return this;
        }

        public Builder wind( int val )
        {
            this.bonus.wind = val;
            return this;
        }

        public Builder light( int val )
        {
            this.bonus.light = val;
            return this;
        }

        public Builder darkness( int val )
        {
            this.bonus.darkness = val;
            return this;
        }

        public Builder elemental( int val )
        {
            this.bonus.physical    = val;
            this.bonus.fire        = val;
            this.bonus.ice         = val;
            this.bonus.electricity = val;
            this.bonus.wind        = val;
            this.bonus.light       = val;
            this.bonus.darkness    = val;
            return this;
        }

        //All bonuses to ailment resistances and counter are tended to below.
        public Builder poison( int val )
        {
            this.bonus.poison = val;
            return this;
        }

        public Builder rage( int val )
        {
            this.bonus.rage = val;
            return this;
        }

        public Builder dizzy( int val )
        {
            this.bonus.dizzy = val;
            return this;
        }

        public Builder confusion( int val )
        {
            this.bonus.confusion = val;
            return this;
        }

        public Builder enervation( int val )
        {
            this.bonus.enervation = val;
            return this;
        }

        public Builder exhaustion( int val )
        {
            this.bonus.exhaustion = val;
            return this;
        }

        public Builder silence( int val )
        {
            this.bonus.silence = val;
            return this;
        }

        public Builder fear( int val )
        {
            this.bonus.fear = val;
            return this;
        }

        public Builder unconscious( int val )
        {
            this.bonus.unconscious = val;
            return this;
        }

        public Builder ailment( int val )
        {
            this.bonus.poison      = val;
            this.bonus.rage        = val;
            this.bonus.dizzy       = val;
            this.bonus.confusion   = val;
            this.bonus.enervation  = val;
            this.bonus.exhaustion  = val;
            this.bonus.silence     = val;
            this.bonus.fear        = val;
            this.bonus.unconscious = val;
            return this;
        }

        public Builder counter( int val )
        {
            this.bonus.counter = val;
            return this;
        }

        public Bonus build()
        {
            return new Bonus( this );
        }
    }

    public Bonus( Builder builder )
    {
        this.strength    = builder.bonus.strength;
        this.magic       = builder.bonus.magic;
        this.endurance   = builder.bonus.endurance;
        this.agility     = builder.bonus.agility;
        this.luck        = builder.bonus.luck;

        this.physical    = builder.bonus.physical;
        this.fire        = builder.bonus.fire;
        this.ice         = builder.bonus.ice;
        this.electricity = builder.bonus.electricity;
        this.wind        = builder.bonus.wind;
        this.light       = builder.bonus.light;
        this.darkness    = builder.bonus.darkness;

        this.poison      = builder.bonus.poison;
        this.rage        = builder.bonus.rage;
        this.dizzy       = builder.bonus.dizzy;
        this.confusion   = builder.bonus.confusion;
        this.enervation  = builder.bonus.enervation;
        this.exhaustion  = builder.bonus.exhaustion;
        this.silence     = builder.bonus.silence;
        this.fear        = builder.bonus.fear;
        this.unconscious = builder.bonus.unconscious;

        this.counter     = builder.bonus.counter;
    }

    public Bonus()
    {
        this.strength    = 0;
        this.magic       = 0;
        this.endurance   = 0;
        this.agility     = 0;
        this.luck        = 0;

        this.physical    = 0;
        this.fire        = 0;
        this.ice         = 0;
        this.electricity = 0;
        this.wind        = 0;
        this.light       = 0;
        this.darkness    = 0;

        this.poison      = 0;
        this.rage        = 0;
        this.dizzy       = 0;
        this.confusion   = 0;
        this.enervation  = 0;
        this.exhaustion  = 0;
        this.silence     = 0;
        this.fear        = 0;
        this.unconscious = 0;

        this.counter     = 0;
    }
}