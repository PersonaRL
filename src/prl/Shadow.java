package prl;

/* I see you're using other classes here like the Stats and Equipment class but they're not imported. Import them so we
 * know exactly what classes are being used here.
 */

public class Shadow
{
    /* I'd declare these as private and provide get/set methods only if we need to access them from outside the class.
     * That's probably the case for those 7 here but the 4 objects sitting in the next bunch might not really need to
     * be accessed outside of the class. Try to find a way to keep it simple and accessible. We don't want to have full
     * access to the objects like we do now and we don't want a ton of get/set methods, maybe just a bunch of useful
     * functions and put in get method only if we need them. Be even more careful when putting a set method.
     */
    String name;
    int    level;
    int    maxHP;
    int    currentHP;
    int    maxSP;
    int    currentSP;
    Arcana arcana;

    Stats         stats;
    Equipment     equipment;
    Resist        resist;
    SpecialResist specialResist;

    /* All in my opinion:
     * The builder has a bit too much and feels cumbersome. I think it should have methods for the main stuff, which it
     * does and then four more for the stats, equipment, resists and speacil resists. All the methods where we set
     * individual resists feel like extra that we might never use. Keeping it simple here would be nice. One idea would
     * be to just have five builder methods or so, one for the shadow's main stats and the rest for the four objects.
     * Instead of setting all resistances in a builder method, we just create a Resist object for example and set it
     * using the builder. Then if we wanna change the shadow's resistance we could have a method belonging to the
     * Shadow class and make it simple to use or something. That way we don't have to deal with lots of methods with
     * long names, just one method and one parameter. That means that Resist will need a nice and simple constructor
     * though.
     *
     * Also, since everything is set here, maybe not now but you should add in error checking especially on the
     * numbers.
     */
    public static class Builder
    {
        //Required fields for creating a shadow object.
        String name;
        int    level;
        int    maxHP;
        int    currentHP;
        int    maxSP;
        int    currentSP;
        Arcana arcana;

        //Optional fields, each set to a default value, which can be optionally changed (hence being optional)
        Stats         stats         = new Stats();         // All stats set to 1.
        Equipment     equipment     = new Equipment();     //No equipment worn.
        Resist        resist        = new Resist();        //All resistances set to 0, except ailments, whose default is 70.
        SpecialResist specialResist = new SpecialResist(); //All special resistances set to NONE.

        public Builder( String name, int level, int HP, int SP, Arcana arcana )
        {
            this.name      = name;
            this.level     = level;
            this.maxHP     = HP;
            this.currentHP = HP;
            this.maxSP     = SP;
            this.currentSP = SP;
            this.arcana    = arcana;
        }

        public Builder stats( int strength, int magic, int endurance, int agility, int luck )
        {
            this.stats.strength  = strength;
            this.stats.magic     = magic;
            this.stats.endurance = endurance;
            this.stats.agility   = agility;
            this.stats.luck      = luck;
            return this;
        }

        public Builder equipment( Weapon weapon, Armor armor, Accessory accessory )
        {
            this.equipment.weapon    = weapon;
            this.equipment.armor     = armor;
            this.equipment.accessory = accessory;
            return this;
        }

        public Builder elementalResist( int physicalResist, int fireResist, int iceResist, int electricityResist, int windResist, int lightResist, int darknessResist )
        {
            this.resist.physical    = physicalResist;
            this.resist.fire        = fireResist;
            this.resist.ice         = iceResist;
            this.resist.electricity = electricityResist;
            this.resist.wind        = windResist;
            this.resist.light       = lightResist;
            this.resist.darkness    = darknessResist;
            return this;
        }

        public Builder elementalResist( int val )
        {
            this.resist.physical    = val;
            this.resist.fire        = val;
            this.resist.ice         = val;
            this.resist.electricity = val;
            this.resist.wind        = val;
            this.resist.light       = val;
            this.resist.darkness    = val;
            return this;
        }

        //All ailment resistances are tended to below.
        public Builder poisonResist( int val )
        {
            this.resist.poison = val;
            return this;
        }

        public Builder rageResist( int val )
        {
            this.resist.rage = val;
            return this;
        }

        public Builder dizzyResist( int val )
        {
            this.resist.dizzy = val;
            return this;
        }

        public Builder confusionResist( int val )
        {
            this.resist.confusion = val;
            return this;
        }

        public Builder enervationResist( int val )
        {
            this.resist.enervation = val;
            return this; }

        public Builder exhaustionResist( int val )
        {
            this.resist.exhaustion = val;
            return this;
        }

        public Builder silenceResist( int val )
        {
            this.resist.silence = val;
            return this;
        }

        public Builder fearResist( int val )
        {
            this.resist.fear = val;
            return this;
        }

        public Builder unconsciousResist( int val )
        {
            this.resist.unconscious = val;
            return this;
        }

        public Builder ailmentResist( int val )
        {
            this.resist.poison      = val;
            this.resist.rage        = val;
            this.resist.dizzy       = val;
            this.resist.confusion   = val;
            this.resist.enervation  = val;
            this.resist.exhaustion  = val;
            this.resist.silence     = val;
            this.resist.fear        = val;
            this.resist.unconscious = val;

            this.resist.counter = val;
            return this;
        }

        //All special resistances are tended to below.
        public Builder specialPhysicalResist( ResistAffinity val )
        {
            this.specialResist.physical = val;
            return this;
        }

        public Builder specialFireResist( ResistAffinity val )
        {
            this.specialResist.fire = val;
            return this;
        }

        public Builder specialIceResist( ResistAffinity val )
        {
            this.specialResist.ice = val;
            return this;
        }

        public Builder specialElectricityResist( ResistAffinity val )
        {
            this.specialResist.electricity = val;
            return this;
        }

        public Builder specialWindResist( ResistAffinity val )
        {
            this.specialResist.wind = val;
            return this;
        }

        public Builder specialLightResist( ResistAffinity val )
        {
            this.specialResist.light = val;
            return this;
        }

        public Builder specialDarknessResist( ResistAffinity val )
        {
            this.specialResist.darkness = val;
            return this;
        }

        public Builder specialResist( ResistAffinity val )
        {
            this.specialResist.physical    = val;
            this.specialResist.fire        = val;
            this.specialResist.ice         = val;
            this.specialResist.electricity = val;
            this.specialResist.wind        = val;
            this.specialResist.light       = val;
            this.specialResist.darkness    = val;
            return this;
        }

        public Shadow build()
        {
            return new Shadow( this );
        }
    }

    public Shadow( Builder builder )
    {
        this.name      = builder.name;
        this.level     = builder.level;
        this.maxHP     = builder.maxHP;
        this.currentHP = builder.currentHP;
        this.maxSP     = builder.maxSP;
        this.currentSP = builder.currentSP;
        this.arcana    = builder.arcana;

        this.stats.strength  = builder.stats.strength;
        this.stats.magic     = builder.stats.magic;
        this.stats.endurance = builder.stats.endurance;
        this.stats.agility   = builder.stats.agility;
        this.stats.luck      = builder.stats.luck;

        this.resist.physical    = builder.resist.physical;
        this.resist.fire        = builder.resist.fire;
        this.resist.ice         = builder.resist.ice;
        this.resist.electricity = builder.resist.electricity;
        this.resist.wind        = builder.resist.wind;
        this.resist.light       = builder.resist.light;
        this.resist.darkness    = builder.resist.darkness;

        this.resist.poison      = builder.resist.poison;
        this.resist.rage        = builder.resist.rage;
        this.resist.dizzy       = builder.resist.dizzy;
        this.resist.confusion   = builder.resist.confusion;
        this.resist.enervation  = builder.resist.enervation;
        this.resist.exhaustion  = builder.resist.exhaustion;
        this.resist.silence     = builder.resist.silence;
        this.resist.fear        = builder.resist.fear;
        this.resist.unconscious = builder.resist.unconscious;

        this.resist.counter     = builder.resist.counter;

        this.specialResist.physical    = builder.specialResist.physical;
        this.specialResist.fire        = builder.specialResist.fire;
        this.specialResist.ice         = builder.specialResist.ice;
        this.specialResist.electricity = builder.specialResist.electricity;
        this.specialResist.wind        = builder.specialResist.wind;
        this.specialResist.light       = builder.specialResist.light;
        this.specialResist.darkness    = builder.specialResist.darkness;
    }
}
