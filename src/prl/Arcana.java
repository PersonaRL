package prl;

public enum Arcana
{
    FOOL,
    MAGICIAN,
    PRIESTESS,
    EMPRESS,
    EMPEROR,
    HIEROPHANT,
    LOVERS,
    CHARIOT,
    JUSTICE,
    HERMIT,
    FORTUNE,
    STRENGTH,
    HANGED_MAN,
    DEATH,
    TEMPERANCE,
    DEVIL,
    TOWER,
    STAR,
    MOON,
    SUN,
    JUDGEMENT,
    WORLD;

    public String toString()
    {
        String text = this.name().replace('_',' ');
        text = text.charAt(0) + text.substring(1).toLowerCase();
        return text;
    }
}
